/*
Name: CSC340-Optional2.cpp
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Driver to test out DoubleLinkedList ADT
*/

#include "DoubleLinkedList.h"

#include <iostream>
#include <random>

//Helper function that returns a random number between a and b
float rand_FloatRange(float a, float b)
{
	return ((b-a)*((float)rand()/RAND_MAX))+a;
}


int main(char* argv)
{
	DoubleLinkedList a, b;
	

	int stopExec = 0;

	for (int i = 0; i < 30; i++)
	{
		a.insert(1, i);
	}

	cout << "Question 1: " << "\n";
	cout << "a: " << a;

	cout << "\n";

	cout << "b: " << b;

	cout << "\n";

	cout << "reversing a: ";
	a.reverse();

	cout << a;

	cout << "\n";
	
	b = a;
	
	cout << b;
	
	cout << "\n";

	cout << "reversing a again: " ;
	a.reverse();
	cout << a;

	cout << "\n";

	


	cin >> stopExec;


	return 0;
}