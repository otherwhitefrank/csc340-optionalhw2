/*
Name: DoubleLinkedList.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Definition of a doubly linked list
*/


#ifndef _DoubleLinkedList_H
#define _DoubleLinkedList_H

#include "ListExceptions.h"
#include "DoubleListNode.h"


/** @class DoubleLinkedList
* ADT DoubleLinkedList - Pointer-based implementation. */
class DoubleLinkedList
{
public:
	
	// Constructors and destructor:

	/** Default constructor. */
	DoubleLinkedList();

	/** Copy constructor.
	* @param aDoubleLinkedList The DoubleLinkedList to copy. */
	DoubleLinkedList(const DoubleLinkedList& aDoubleLinkedList);

	//overload the assignment operator
	DoubleLinkedList& operator=(const DoubleLinkedList&  rhs);
	/** Destructor. */
	~DoubleLinkedList();

	// DoubleLinkedList operations:
	bool isEmpty() const;
	int getLength() const;
	void insert(int index, const ListItemType& newItem)
		throw(ListIndexOutOfRangeException, ListException);
	void remove(int index)
		throw(ListIndexOutOfRangeException);
	void retrieve(int index, ListItemType& dataItem) const
		throw(ListIndexOutOfRangeException);

	
	
	void reverse(); //Reverses DoubleLinkedList

	void clearList(); //Wipes DoubleLinkedList

	//overload the << operator: idea is similar to the find()
	friend ostream & operator << (ostream & out, DoubleLinkedList& aDoubleLinkedList);
protected:
 //Make these protected so SortedDoubleLinkedList can access them as a child

	/** Number of items in DoubleLinkedList. */
	int      size;
	/** Pointer to linked DoubleLinkedList of items. */
	DoubleListNode *head;
	DoubleListNode *tail;

	//Print function used by operator <<
	void PrintList(ostream& out);

private:

	/** Locates a specified node in a linked DoubleLinkedList.
	* @pre index is the number of the desired node.
	* @post None.
	* @param index The index of the node to locate.
	* @return A pointer to the index-th node. If index < 1
	*        or index > the number of nodes in the DoubleLinkedList,
	*        returns NULL. */
	DoubleListNode *find(int index) const;
}; // end DoubleLinkedList
// End of header file.

#endif