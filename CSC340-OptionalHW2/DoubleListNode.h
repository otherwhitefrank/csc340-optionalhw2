/*
Name: DoubleListNode.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: define the data structure DoubleListNode that will be used by our doubly linked list
*/

#ifndef _DOUBLELIST_NODE_ITERATOR
#define _DOUBLELIST_NODE_ITERATOR

#include <iostream> //Brings across NULL, needed for standard constructor

typedef int ListItemType;

class DoubleListNode
{
public:

	DoubleListNode(ListItemType itemVal, DoubleListNode *nodePtr):item(itemVal),next(nodePtr){}; //Standard Constructor with values
	DoubleListNode():item(0),next(NULL){}; //Default constructor
	
	ListItemType item; //Current Item
	DoubleListNode* next; //Next item
	DoubleListNode* prev;
};

#endif