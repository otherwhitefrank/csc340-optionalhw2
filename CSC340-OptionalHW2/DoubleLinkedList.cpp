/*
Name: DoubleLinkedList.cpp
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Implementation of a single linked List class DoubleLinkedList
*/

#include "DoubleLinkedList.h"
#include "DoubleListNode.h"

using namespace std;

DoubleLinkedList::DoubleLinkedList():size(0),head(NULL),tail(NULL) //initializer or initialization segment
{
}

DoubleLinkedList::DoubleLinkedList(const DoubleLinkedList& aDoubleLinkedList)
	: size(aDoubleLinkedList.size)
{
	//Empty list passed in
	if (aDoubleLinkedList.head == NULL && aDoubleLinkedList.tail == NULL){
		head = NULL;  // original DoubleLinkedList is empty
		tail = NULL;
		size = 0;
	}
	else
	{  // copy first node
		tail = head = new DoubleListNode;

		head->item = aDoubleLinkedList.head->item;

		//Set heads prev to NULL
		head->prev = NULL;

		// copy rest of DoubleLinkedList
		DoubleListNode *newPtr = head;  // new DoubleLinkedList pointer
		// newPtr points to last node in new DoubleLinkedList
		// origPtr points to nodes in original DoubleLinkedList
		for (DoubleListNode *origPtr = aDoubleLinkedList.head->next;
			origPtr != NULL;
			origPtr = origPtr->next)
		{  
			newPtr->next = new DoubleListNode;
			newPtr->next->prev = newPtr;
			newPtr = newPtr->next;
			newPtr->item = origPtr->item;
		}  // end for


		newPtr->next = NULL;
	}  // end if
}  // end copy constructor

DoubleLinkedList::~DoubleLinkedList()
{
	//while (!isEmpty())
	//  remove(1);
	DoubleListNode *curr=head;
	while (curr!=NULL){
		head = curr->next;
		delete curr;
		curr = head;
	}

}  // end destructor

bool DoubleLinkedList::isEmpty() const
{
	return size == 0;
}  // end isEmpty

int DoubleLinkedList::getLength() const
{
	return size;
}  // end getLength

DoubleListNode* DoubleLinkedList::find(int index) const
{
	if ( (index < 1) || (index > getLength()) )
		return NULL;

	else  // count from the beginning of the DoubleLinkedList.
	{  DoubleListNode *cur = head;
	for (int skip = 1; skip < index; ++skip)
		cur = cur->next;
	return cur;
	}  // end if

}  // end find

void DoubleLinkedList::retrieve(int index,
								ListItemType& dataItem) const
								throw(ListIndexOutOfRangeException)
{
	if ( (index < 1) || (index > getLength()) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: retrieve index out of range");
	else
	{  // get pointer to node, then data in node
		DoubleListNode *cur = find(index);
		dataItem = cur->item;
	}  // end if
}  // end retrieve

void DoubleLinkedList::insert(int index, const ListItemType& newItem)
	throw(ListIndexOutOfRangeException, ListException)
{
	int newLength = getLength() + 1;

	if ( (index < 1) || (index > newLength) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: insert index out of range");
	else
	{  // try to create new node and place newItem in it
		try
		{
			DoubleListNode *newPtr = new DoubleListNode;
			size = newLength;
			newPtr->item = newItem;

			// attach new node to DoubleLinkedList
			if (index == 1)
			{  // insert new node at beginning of DoubleLinkedList
				if (head == NULL && tail == NULL) //Empty list
				{
					head = tail = newPtr;
					newPtr->next = newPtr->prev = NULL;
				}
				else
				{
					head->prev = newPtr;
					newPtr->next = head;
					head = newPtr;
					newPtr->prev = NULL;
				}
			}
			else
			{  
				DoubleListNode* cur = find(index-1);
				if (cur != NULL)
				{
					newPtr->prev = cur;
					newPtr->next = cur->next;
					cur->next = newPtr;
				}
				else
				{
					throw ListException("Error, can't find index to insert at!");
				}  // end if
			}
		}  // end try
		catch (bad_alloc e)
		{
			throw ListException(
				"ListExceptionn: memory allocation failed on insert");
		}  // end catch
	}  // end if
}  // end insert

void DoubleLinkedList::remove(int index) throw(ListIndexOutOfRangeException)
{
	DoubleListNode *cur;

	if ( (index < 1) || (index > getLength()) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: remove index out of range");
	else
	{  
		--size;

		if (index == 1)
		{  // delete the first node from the DoubleLinkedList
			cur = head;  // save pointer to node
			head = head->next;	
			head->prev = NULL;
		}

		else
		{  
			cur = find(index);
			cur->prev->next = cur->next;
			cur->next->prev = cur->prev;
			
			
		}  // end if

		
		delete cur;
		cur = NULL;
	}  // end if
}  // end remove


void DoubleLinkedList::PrintList(ostream& out)
{
	DoubleListNode* curr;
	curr = this->head;

	while (curr != NULL)
	{
		out << curr->item << " ";
		curr = curr->next;
	}
}

//overload the assignment operator
DoubleLinkedList& DoubleLinkedList::operator=(const DoubleLinkedList&  rhs)
{
	int newSize = rhs.size;

	this->clearList();


	for (int i = 1 ; i < newSize+1; i++)
	{
		ListItemType a;
		rhs.retrieve(i, a);
		this->insert(i, a);
	}

	return *this;
}


void DoubleLinkedList::clearList() //Wipes DoubleLinkedList
{
	DoubleListNode* cur = this->head;
	DoubleListNode* prev = NULL;
	while (cur != NULL)
	{
		prev = cur;
		cur = cur->next;
		delete prev;
	}
	size = 0;
	head = tail = NULL;

}

//overload the << operator: idea is similar to the find()
ostream& operator<< (ostream & out, DoubleLinkedList & aDoubleLinkedList)
{
	aDoubleLinkedList.PrintList(out);

	return out;
}


//Reverses DoubleLinkedList
void DoubleLinkedList::reverse()
{
	DoubleListNode* cur = NULL;
	

	//swap all of our next and prev pointers
	for (cur = head; cur != NULL; cur = cur->prev)
	{
		//Swap prev and next pointers
		DoubleListNode* temp = cur->prev;
		cur->prev = cur->next;
		cur->next = temp;
	}

	cur = tail;
	tail = head;
	head = cur;
}

